function Traveler (name, food, isHealthy){
    this.name = name;
    this.food = 1;
    this.isHealthy = true;
}

function Wagon (capacity, passengers){
    this.capacity = capacity;
    this.passengers = [];
    this.emptySeats = function(){
       return this.capacity - this.passengers.length
    }
}


Traveler.prototype.hunt = function(){
    return this.food+=2
}

Traveler.prototype.eat = function(){
    this.food-=1;
    if(this.food === 0){
        this.isHealthy = false
    }
}

Wagon.prototype.getAvailableSeatCount = function(){
    return this.capacity - this.passengers.length
}


Wagon.prototype.join = function(Traveler){
    let array = this.passengers 
    if (this.emptySeats() > 0){
        array.push(Traveler)
        return array
    }else{
        console.log("There is no more room!")
    }
}

Wagon.prototype.shouldQuarantine = function(){
    for(i=0;i<this.passengers.length;i++){
    if(this.passengers[i].isHealthy === true){
        return true
    }else{
        return false
    }
}
}

Wagon.prototype.totalFood = function(){
    let foodTotal = 0;
    for(let i=0;i<this.passengers.length;i++){
        foodTotal += this.passengers[i].food
        }
        return foodTotal
    }
 



// Create a wagon that can hold 2 people
let wagon = new Wagon(2);
// Create three travelers
let henrietta = new Traveler('Henrietta');
let juan = new Traveler('Juan');
let maude = new Traveler('Maude');
console.log(`${wagon.getAvailableSeatCount()} should be 2`);
wagon.join(henrietta);
console.log(`${wagon.getAvailableSeatCount()} should be 1`);
wagon.join(juan);
wagon.join(maude); // There isn't room for her!
console.log(`${wagon.getAvailableSeatCount()} should be 0`);
henrietta.hunt(); // get more food
// juan.eat();
juan.eat(); // juan is now hungry (sick)
console.log(`${wagon.shouldQuarantine()} should be true since juan is sick`);
console.log(`${wagon.totalFood()} should be 3`);